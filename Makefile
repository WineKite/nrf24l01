PROJECT_NAME := nrf24l01

all:
	gcc ${PROJECT_NAME}_main.c  NRF24L01.c -I .  -o ${PROJECT_NAME} -L/usr/local/lib -lwiringPi -lwiringPiDev -lpthread -lm -lcrypt -lrt

clean:
	rm -f ${PROJECT_NAME}