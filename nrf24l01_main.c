#include <stdio.h>
#include <wiringPi.h>
#include "NRF24L01.h"
#include <stdint.h>
#include <unistd.h>

int main()
{
#if 1//发送模式
    NRF24L01_Init();
    uint8_t Buf[32] = {5, 0x12, 0x22, 0x33, 0x44, 0x55}; //第零个数必须是要发送的数据个数，不包括本身
    int timer=10;
    printf("开始发送\r\n剩余次数10");
    while(timer--)
    {
        NRF24L01_SendBuf(Buf);
        printf("\r剩余次数%02d",timer);
        fflush(stdout);
        delay(100);
    }
    printf("\n",timer);
    return 0;
#endif

#if 0 //接收模式
    uint8_t Buf[32] = {0};
	NRF24L01_Init();
    while (1)
	{
		if (NRF24L01_Get_Value_Flag() == 0)
		{
			NRF24L01_GetRxBuf(Buf);
            Buf[Buf[0]+1] = 0;
            printf("recv:%s\r\n",Buf+1);
		}
        delay(10);
	}
#endif

}



